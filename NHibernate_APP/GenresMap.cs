﻿using FluentNHibernate.Mapping;
namespace NHibernate_APP
{
    class GenresMap:ClassMap<Genres>
    {
        public GenresMap()
        {
            Table("Genres");
            Id(x => x.GenreName);

            HasManyToMany(x => x.songs).Table("GenresInSongs").Inverse().ParentKeyColumn("GenreName").ChildKeyColumns.Add("SingerName", "SongName").Cascade.SaveUpdate().AsBag();
        }
    }
}
