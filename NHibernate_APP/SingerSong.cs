﻿using System.Collections.Generic;
namespace NHibernate_APP
{
  public class SingerSong
    {
        public virtual string SingerName { get; set; }
        public virtual string SongName { get; set; }
        public virtual int Duration { get; set; }
        public virtual IList<Genres> genres { get; set; }
        public virtual IList<Albums> albums { get; set; }
        public SingerSong()
        {
        }

        public SingerSong(string singername, string songname, int duration)
        {
            SingerName = singername;
            SongName = songname;
            Duration = duration;
            genres = new List<Genres>();
            albums = new List<Albums>();
        }

        public override int GetHashCode()
        {
            return (SingerName + "|" + SongName).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is SingerSong)
            {
                var toCompare = obj as SingerSong;
                return this.GetHashCode() != toCompare.GetHashCode();
            }
            return false;
        }

    }
}
