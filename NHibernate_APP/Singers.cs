﻿using System.Collections.Generic;

namespace NHibernate_APP
{
   public class Singers
    {
        public virtual string SingerName { get; set; }
        public virtual IList<SingerSong> Songs { get; set; }
    

        public Singers()
        {
 
        }

        public Singers(string name)
        {
            SingerName = name;
           
            Songs = new List<SingerSong>();
        }

    }
}
