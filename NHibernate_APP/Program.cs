﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;

namespace NHibernate_APP
{
    class Program
    {
        static void Main(string[] args)
        {
            FluentConfiguration config = Fluently
               .Configure()
               .Database(MsSqlConfiguration
                             .MsSql2008
                             .ConnectionString(@"Data Source=антон-пк\sqlexpress;Initial Catalog=NHIB_DB;Integrated Security=True;Pooling=False"))
                             .Mappings(configuration => configuration
                             .FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()));
          
            var export = new SchemaUpdate(config.BuildConfiguration());
            export.Execute(true, true);
            ISessionFactory sessionFactory = config.BuildSessionFactory();


            string[] lines = System.IO.File.ReadAllLines(@".\WriteText.txt");
            foreach (string line in lines)
            {
                if (line != "")
                {
                    string AlbumName=null;
                    string[] parts = line.Split(';');
                    string[] info = parts[0].Split('.', '-','(',')');
                    string SongName = info[1];
                    string SingerName = info[2];
                    if (info.Length>3) {AlbumName = info[3]; }
                    string[] genres = parts[2].Split(',');



                   using (ISession session = sessionFactory.OpenSession())
                    {

                        using (var transaction = session.BeginTransaction())
                        {
                         var newSinger = new Singers(SingerName);                         
                         var newGenres = new List<Genres>();
                            foreach (var genr in genres)
                           {
                               var g = new Genres(genr);
                               newGenres.Add(g);
                               session.SaveOrUpdate(g);
                           }                                                                          
                              var newSong = new SingerSong(SingerName, SongName, 138);
                        newSong.genres = newGenres;
                        newSinger.Songs.Add(newSong);
                              if (AlbumName != null)
                            {
                            var newAlbum = new Albums(info[3]);
                            newAlbum.Songs.Add(newSong);                        
                            session.SaveOrUpdate(newAlbum);
                            }                         
                            session.SaveOrUpdate(newSong);                                                                         
                            transaction.Commit();
                        }
                    }
                }
            }

            using (ISession session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
             
                   var newQ = session.QueryOver<SingerSong>()
                      .JoinQueryOver<Genres>(x=>x.genres)
                      .Where(Restrictions.Disjunction().Add(Restrictions.On<Genres>(x=>x.GenreName).IsLike(" Rock")))
                       ;
                   var res = newQ.List();
                   SingerSong ss = null;
                   Genres gn = null;


      // Все альбомы указанной группы.
                   var q2 = session.QueryOver<Albums>()
                       .JoinQueryOver<SingerSong>(x => x.Songs)
                       .Where(x => x.SingerName == " Linkin Park ");
                   var q4 = q2.List();
                   foreach (Albums qqq in q4) { Console.WriteLine(qqq.AlbumName); }
                 
      //Все песни из всех альбомов указанной группы.
                   var w4 = session.QueryOver<SingerSong>()
                       .Where(x=>x.SingerName==" Linkin Park ")
                       .JoinQueryOver<Albums>(x => x.albums)
                       .List();
                   foreach (SingerSong qqq in w4) { Console.WriteLine(qqq.SongName); }

      // Все пары групп, у которых есть одноименные песни.
                  IList<object[]> r1 = session.QueryOver<SingerSong>()
                      .Select(Projections.GroupProperty
                      (Projections.Property<SingerSong>(x => x.SongName))
                      ,Projections.Count<SingerSong>(x => x.SongName))
                      .Where(Restrictions.Gt( Projections.Count<SingerSong>(x => x.SongName),1))
                      .List<object[]>();
              

      // Все песни указанной группы с длительностью, больше указанной.
                  IList<object> r4 = session.QueryOver<SingerSong>()
                      .Where(x => x.SingerName == " Linkin Park ")
                      .And(x => x.Duration > 120)
                      .List<object>();
                        
      //  Все песни из указанного альбома короче 3 минут.
                  IList<object> r5 = session.QueryOver<SingerSong>()
                    .Where(x => x.SingerName == " Linkin Park ")
                    .And(x => x.Duration > 120)
                    .JoinQueryOver<Albums>(c=>c.albums)
                    .Where(x=>x.AlbumName == "Meteora")
                    .List<object>();

     // Все песни, указанного жанра.
                  IList<object> r6 = session.QueryOver<SingerSong>()
                      .JoinQueryOver<Genres>(x => x.genres)
                      .Where(x => x.GenreName == " Metal")
                      .List<object>();
     //  Все песни, являющиеся одновременно песнями нескольких указанных жанров.

                  string awp = " Rock";
                  string awp2 = " Pop-Rock";
                    IList<object> r12 = session.QueryOver<SingerSong>(()=>ss) 
                    .JoinAlias(()=>ss.genres,()=>gn)
                    .Where(() => gn.GenreName == awp || gn.GenreName ==awp2 )
                    .Select(Projections.GroupProperty
                    (Projections.Property<SingerSong>(x => x.SongName))
                    ,Projections.Count<SingerSong>(x => x.SongName))
                    .Where(Restrictions.Gt(Projections.Count<SingerSong>(x => x.SongName), 1))
                    .List<object>();

                        

      //  Все песни, являющиеся одним из указанных жанров.
                  IList<object> r7 = session.QueryOver<SingerSong>()
                      .JoinQueryOver<Genres>(x => x.genres)
                      .Where(Restrictions.Disjunction()
                      .Add(Restrictions.On<Genres>(x => x.GenreName).IsLike(" Metal")
                        || Restrictions.On<Genres>(x => x.GenreName).IsLike(" Rock")))                     
                      .List<object>();
      //  Все альбомы с указанием преобладающего жанра (жанров, если количество песен совпадает).
                  IList<object> r8 = session.QueryOver<Albums>()
                      .JoinQueryOver<SingerSong>(x => x.Songs)
                      .JoinQueryOver<Genres>(x => x.genres)
                      .Select(Projections.GroupProperty(Projections.Property<Albums>(x=>x.AlbumName)))
                      .List<object>();


                  IList<object> r11 = (session.QueryOver<Albums>()
                      .JoinQueryOver<SingerSong>(x => x.Songs)
                      .JoinQueryOver<Genres>(x => x.genres))
                      .Select(Projections.ProjectionList()
                      .Add(Projections.Group<Albums>(x => x.AlbumName), "AlbumName"))
                      .List<object>();
     //  Жанр, преобладающий во всем трэк-листе.
                  IList<object[]> r9 = (session.QueryOver<Genres>()
                      .JoinQueryOver<SingerSong>(x => x.songs))
                      .Select(Projections.GroupProperty
                      (Projections.Property<Genres>(x => x.GenreName))
                      ,Projections.Count<Genres>(x => x.GenreName))
                      .OrderBy(Projections.Count<SingerSong>(x=>x.SingerName))
                      .Desc
                      .Take(1)                  
                      .List<object[]>();
                  Console.WriteLine("The most populat genge is "+r9[0][0]);
      // Рейтинг жанров трэк-листа начиная с самого распространенного.
                  IList<object[]> r10 = (session.QueryOver<Genres>()
                     .JoinQueryOver<SingerSong>(x => x.songs))                  
                     .Select(Projections.ProjectionList()
                     .Add(Projections.Group<Genres>(x => x.GenreName),"GenreName")
                     .Add(Projections.Count<Genres>(x => x.GenreName),"count")) 
                     .UnderlyingCriteria.AddOrder(new Order ("count",false))                     
                     .List<object[]>();
                  foreach (var name in r10) { Console.WriteLine(name[0]); }
       //  Песни уникального жанра (только 1 песня данного жанра).

                  IList<object[]> r13 = session.QueryOver<Genres>(() => gn)
                      .JoinAlias(() => gn.songs, () => ss)
                      .Select(Projections.GroupProperty
                      (Projections.Property<Genres>(x => x.GenreName))
                      ,Projections.Count<Genres>(x => x.GenreName))
                      .Where(Restrictions.Eq(Projections.Count<Genres>(x => x.GenreName), 1))
                      .List<object[]>();
                  Console.WriteLine("Uniqe genres : ");
                  foreach (var gname in r13) { Console.WriteLine(gname[0]);}

       //  Количество песен, не содержащих указанный жанр.

                  IList<string> r14 = session.QueryOver<SingerSong>(() => ss)
                      .JoinAlias(() =>ss.genres, () => gn)                     
                      .Where(() => gn.GenreName == " Rock")
                       .Select(x => x.SongName).List<string>();
                  r14.ToList<string>();
                  var r15 = session.QueryOver<SingerSong>();
                    
                   
       //  Количество песен, не лежащих в альбомах.


                }
            }







            
        }
    }
}
