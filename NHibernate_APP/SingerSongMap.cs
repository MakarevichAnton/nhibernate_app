﻿using FluentNHibernate.Mapping;

namespace NHibernate_APP
{
    class SingerSongMap : ClassMap<SingerSong>
    {
        SingerSongMap()
        {
            Table("SingerSong");
            CompositeId().KeyProperty(x => x.SingerName).KeyProperty(x => x.SongName);
            Map(x => x.Duration);

            HasManyToMany(x => x.genres).Table("GenresInSongs").ParentKeyColumns.Add("SingerName","SongName").ChildKeyColumn("GenreName").Cascade.SaveUpdate().AsBag();
            HasManyToMany(x => x.albums).Table("SongsInAlbums").Inverse().ParentKeyColumns.Add("SingerName", "SongName").ChildKeyColumn("AlbumName").Cascade.SaveUpdate().AsBag();
        }
    }
}
