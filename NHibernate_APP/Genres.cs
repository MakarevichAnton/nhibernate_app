﻿using System.Collections.Generic;


namespace NHibernate_APP
{
   public class Genres
    {
        public virtual string GenreName { get; set; }
        public virtual IList<SingerSong> songs { get; set; }
        public Genres()
        { 
        }

        public Genres(string genrename)
        {
            GenreName = genrename;
        }
    }
}
