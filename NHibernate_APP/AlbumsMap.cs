﻿using FluentNHibernate.Mapping;

namespace NHibernate_APP
{
    class AlbumsMap: ClassMap<Albums>
    {
        public AlbumsMap()
        {
            Table("Albums");
            Id(x => x.AlbumName);

            HasManyToMany(x => x.Songs).Table("SongsInAlbums").ParentKeyColumn("AlbumName").ChildKeyColumns.Add("SingerName","SongName").Cascade.SaveUpdate();
        }
    }
}
