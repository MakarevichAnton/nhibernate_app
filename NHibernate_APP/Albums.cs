﻿using System.Collections.Generic;
namespace NHibernate_APP

{
    public class Albums
    {
        public virtual string AlbumName { get; set; }

        public virtual IList<SingerSong> Songs { get; set; } 

        public Albums()
        { }

        public Albums(string albumname)
        {
            AlbumName = albumname;
        
            Songs = new List<SingerSong>();
        }
    }
}
